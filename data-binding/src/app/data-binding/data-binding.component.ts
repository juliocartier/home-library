import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {

  url: string = 'https://github.com/juliocartier';
  cursoAngular = true;

  urlImage = 'http://lorempixel.com/400/200/sports';

  valorAtual: string = '';
  valorSalvo: string;

  isMouseOver: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  getValor(){
    return 4;
  }

  getCurtirCurso(){
    return true;
  }

  botaoClicado(){
    alert('Botao Clicado');
  }

  onKeyUp(evento: KeyboardEvent){
   // console.log((<HTMLInputElement>evento.target).value);

   this.valorAtual = (<HTMLInputElement>evento.target).value;
  }

  salvarValor(valor){
    this.valorSalvo = valor;

  }

  onMouseOverOut(){
    this.isMouseOver = !this.isMouseOver;
  }

}
