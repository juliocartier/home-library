import { Component, OnInit } from '@angular/core';
import { CursosService } from './cursos.service';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.scss']
})
export class CursosComponent implements OnInit {

  nomePortal: string = '';
  cursos: string[];

  constructor(public cursoService: CursosService) { 
    this.nomePortal = 'https://github.com/juliocartier';

    this.cursos = this.cursoService.getCursos();


  }

  ngOnInit(): void {
  }

}
